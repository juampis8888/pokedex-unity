using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PokemonInfo 
{
    public string Name;

    public Texture2D Sprite;

    public Texture2D Stats;

    public List<string> Evolution;

    public List<string> Ability;

    public List<TypePokemon> Debility;

    public List<TypePokemon> TypePokemon;
}
