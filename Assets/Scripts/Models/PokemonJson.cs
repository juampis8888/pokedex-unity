using Newtonsoft.Json;
using System;
using System.Collections.Generic;

[Serializable]
public class Ability2
{
    public string name;
    public string url;
}

[Serializable]
public class Ability
{
    public Ability2 ability;
    public bool is_hidden;
    public int slot;
}

[Serializable]
public class Form
{
    public string name;
    public string url;
}

[Serializable]
public class Version
{
    public string name;
    public string url;
}

[Serializable]
public class GameIndice
{
    public int game_index;
    public Version version;
}

[Serializable]
public class MoveLearnMethod
{
    public string name;
    public string url;
}

[Serializable]
public class VersionGroup
{
    public string name;
    public string url;
}

[Serializable]
public class VersionGroupDetail
{
    public int level_learned_at;
    public MoveLearnMethod move_learn_method;
    public VersionGroup version_group;
}

[Serializable]
public class Move2
{
    public string name;
    public string url;
}

[Serializable]
public class Move
{
    public Move2 move;
    public List<VersionGroupDetail> version_group_details;
}

[Serializable]
public class Species
{
    public string name;
    public string url;
}

[Serializable]
public class DreamWorld
{
    public string front_default;
    public object front_female;
}

[Serializable]
public class Home
{
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class OfficialArtwork
{
    public string front_default;
}

[Serializable]
public class Other
{
    public DreamWorld dream_world;
    public Home home;

    [JsonProperty("official-artwork")]
    public OfficialArtwork OfficialArtwork;
}

[Serializable]
public class RedBlue
{
    public string back_default;
    public string back_gray;
    public string back_transparent;
    public string front_default;
    public string front_gray;
    public string front_transparent;
}

[Serializable]
public class Yellow
{
    public string back_default;
    public string back_gray;
    public string back_transparent;
    public string front_default;
    public string front_gray;
    public string front_transparent;
}

[Serializable]
public class GenerationI
{
    [JsonProperty("red-blue")]
    public RedBlue RedBlue;
    public Yellow yellow;
}

[Serializable]
public class Crystal
{
    public string back_default;
    public string back_shiny;
    public string back_shiny_transparent;
    public string back_transparent;
    public string front_default;
    public string front_shiny;
    public string front_shiny_transparent;
    public string front_transparent;
}

[Serializable]
public class Gold
{
    public string back_default;
    public string back_shiny;
    public string front_default;
    public string front_shiny;
    public string front_transparent;
}

[Serializable]
public class Silver
{
    public string back_default;
    public string back_shiny;
    public string front_default;
    public string front_shiny;
    public string front_transparent;
}

[Serializable]
public class GenerationIi
{
    public Crystal crystal;
    public Gold gold;
    public Silver silver;
}

[Serializable]
public class Emerald
{
    public string front_default;
    public string front_shiny;
}

[Serializable]
public class FireredLeafgreen
{
    public string back_default;
    public string back_shiny;
    public string front_default;
    public string front_shiny;
}

[Serializable]
public class RubySapphire
{
    public string back_default;
    public string back_shiny;
    public string front_default;
    public string front_shiny;
}

[Serializable]
public class GenerationIii
{
    public Emerald emerald;

    [JsonProperty("firered-leafgreen")]
    public FireredLeafgreen FireredLeafgreen;

    [JsonProperty("ruby-sapphire")]
    public RubySapphire RubySapphire;
}

[Serializable]
public class DiamondPearl
{
    public string back_default;
    public object back_female;
    public string back_shiny;
    public object back_shiny_female;
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class HeartgoldSoulsilver
{
    public string back_default;
    public object back_female;
    public string back_shiny;
    public object back_shiny_female;
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class Platinum
{
    public string back_default;
    public object back_female;
    public string back_shiny;
    public object back_shiny_female;
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class GenerationIv
{
    [JsonProperty("diamond-pearl")]
    public DiamondPearl DiamondPearl;

    [JsonProperty("heartgold-soulsilver")]
    public HeartgoldSoulsilver HeartgoldSoulsilver;
    public Platinum platinum;
}

[Serializable]
public class Animated
{
    public string back_default;
    public object back_female;
    public string back_shiny;
    public object back_shiny_female;
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class BlackWhite
{
    public Animated animated;
    public string back_default;
    public object back_female;
    public string back_shiny;
    public object back_shiny_female;
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class GenerationV
{
    [JsonProperty("black-white")]
    public BlackWhite BlackWhite;
}

[Serializable]
public class OmegarubyAlphasapphire
{
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class XY
{
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class GenerationVi
{
    [JsonProperty("omegaruby-alphasapphire")]
    public OmegarubyAlphasapphire OmegarubyAlphasapphire;

    [JsonProperty("x-y")]
    public XY XY;
}

[Serializable]
public class Icons
{
    public string front_default;
    public object front_female;
}

[Serializable]
public class UltraSunUltraMoon
{
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
}

[Serializable]
public class GenerationVii
{
    public Icons icons;

    [JsonProperty("ultra-sun-ultra-moon")]
    public UltraSunUltraMoon UltraSunUltraMoon;
}

[Serializable]
public class GenerationViii
{
    public Icons icons;
}

[Serializable]
public class Versions
{
    [JsonProperty("generation-i")]
    public GenerationI GenerationI;

    [JsonProperty("generation-ii")]
    public GenerationIi GenerationIi;

    [JsonProperty("generation-iii")]
    public GenerationIii GenerationIii;

    [JsonProperty("generation-iv")]
    public GenerationIv GenerationIv;

    [JsonProperty("generation-v")]
    public GenerationV GenerationV;

    [JsonProperty("generation-vi")]
    public GenerationVi GenerationVi;

    [JsonProperty("generation-vii")]
    public GenerationVii GenerationVii;

    [JsonProperty("generation-viii")]
    public GenerationViii GenerationViii;
}

[Serializable]
public class Sprites
{
    public string back_default;
    public object back_female;
    public string back_shiny;
    public object back_shiny_female;
    public string front_default;
    public object front_female;
    public string front_shiny;
    public object front_shiny_female;
    public Other other;
    public Versions versions;
}

[Serializable]
public class Stat2
{
    public string name;
    public string url;
}

[Serializable]
public class Stat
{
    public int base_stat;
    public int effort;
    public Stat2 stat;
}

[Serializable]
public class Type2
{
    public string name;
    public string url;
}

[Serializable]
public class Type
{
    public int slot;
    public Type2 type;
}

[Serializable]
public class PokemonJson
{
    public List<Ability> abilities;
    public int base_experience;
    public List<Form> forms;
    public List<GameIndice> game_indices;
    public int height;
    public List<object> held_items;
    public int id;
    public bool is_default;
    public string location_area_encounters;
    public List<Move> moves;
    public string name;
    public int order;
    public List<object> past_types;
    public Species species;
    public Sprites sprites;
    public List<Stat> stats;
    public List<Type> types;
    public int weight;
}