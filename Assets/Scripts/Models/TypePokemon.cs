using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TypePokemon
{
    public Texture2D TextureType;

    public string NameType;
}
