using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using TMPro;

public class AdapterListPokemon : MonoBehaviour
{
    public string Name;

    public TextMeshProUGUI NameText;

    public RawImage RawImage;

    public Button ButtonGame;

    public void Parent(Transform Parent)
    {
        transform.SetParent(Parent);
    }

    public void Location(float topY)
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.localPosition = new Vector3(0, topY, 0);
    }

    public void SetRawImage(Texture2D sprite)
    {
        RawImage.texture = sprite;
    }
}
