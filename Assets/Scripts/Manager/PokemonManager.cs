using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PokemonManager : MonoBehaviour
{
    public ShowInfoManager ShowInfoManager;

    public List<Pokemon> Pokemons;

    public List<Texture2D> PokemonTexture;

    public List<AdapterListPokemon> adapterListPokemons;

    public RectTransform ContentListAddPokemon;

    public RectTransform ContentListPokemon;

    public Transform PanelAddPokemon;

    public Transform PanelPokemon;

    public Transform PanelShowInfo;

    public AdapterListPokemon AdapterListAddPokemon;

    public string URLImage;

    // Start is called before the first frame update
    void Start()
    {
        CreateListPokemon();
        CreateListAddPokemon();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateListAddPokemon()
    {
        var ContentRectTransform = AdapterListAddPokemon.GetComponent<RectTransform>();
        float templateHeight = ContentRectTransform.rect.height;
        float TopY = 0;
        int Position = 0, Count = 0;
        Pokemons.ForEach(pokemon =>
        {
            if (Position > 2)
            {
                var item = Instantiate(AdapterListAddPokemon);
                item.name = string.Format("{0}. {1}", pokemon.index, pokemon.Name);
                item.Name = pokemon.Name;
                item.NameText.text = string.Format("{0}. {1}", pokemon.index, pokemon.Name);
                item.ButtonGame.onClick.AddListener(() =>
                {
                    CreatePokemon(pokemon);
                    UpdateListAddPokemon(pokemon);
                    MovePanelAddPokemon(-1920);
                    MovePanelPokemon(0);

                });
                TopY = TopY = (((Position * templateHeight) + (20 * (Position + 1))) * -1) + 620;
                item.Parent(ContentListAddPokemon);
                item.SetRawImage(PokemonTexture[Position]);
                item.Location(TopY);
                item.transform.localScale = new Vector3(1f, 1f, 1f);
                adapterListPokemons.Insert(Count, item);
                Count++;
            }
            Position++;
            
        });
        var Height = ((Position * templateHeight) + (20 * (Position + 1)));
        ContentListAddPokemon.sizeDelta = new Vector2(ContentListAddPokemon.sizeDelta.x, Height);
    }

    public void CreateListPokemon()
    {
        var ContentRectTransform = AdapterListAddPokemon.GetComponent<RectTransform>();
        float templateHeight = ContentRectTransform.rect.height;
        float TopY = 0;
        int Position = 0;
        Pokemons.ForEach(pokemon =>
        {
            if (Position >= 0 & Position < 3 )
            {
                var item = Instantiate(AdapterListAddPokemon);
                item.name = string.Format("{0} {1}", pokemon.index, pokemon.Name);
                item.NameText.text = string.Format("{0}. {1}", pokemon.index, pokemon.Name);
                item.ButtonGame.onClick.AddListener(() =>
                {
                    ShowInfoManager.ShowInfoPokemon(pokemon.index - 1);
                    MovePanelAddPokemon(-1920);
                    MovePanelPokemon(-1920);
                    MovePanelShowInfo(0);

                });
                TopY = TopY = (((Position * templateHeight) + (20 * (Position + 1))) * -1);
                item.Parent(ContentListPokemon);
                item.SetRawImage(PokemonTexture[Position]);
                item.Location(TopY);
                item.transform.localScale = new Vector3(1f, 1f, 1f);
                Position++;
            }
            
        });
        var Height = (((Position * templateHeight) + (20 * (Position + 1))));
        ContentListPokemon.sizeDelta = new Vector2(ContentListPokemon.sizeDelta.x, Height);
    }

    public void CreatePokemon(Pokemon pokemon)
    {
        var ContentRectTransform = AdapterListAddPokemon.GetComponent<RectTransform>();
        float templateHeight = ContentRectTransform.rect.height;
        float TopY = 0;
        int Position = ContentListPokemon.childCount;
        var item = Instantiate(AdapterListAddPokemon);
        item.name = string.Format("{0} {1}", pokemon.index, pokemon.Name);
        item.Name = pokemon.Name;
        item.NameText.text = string.Format("{0}. {1}", pokemon.index, pokemon.Name);
        item.ButtonGame.onClick.AddListener(() =>
        {
            ShowInfoManager.ShowInfoPokemon(pokemon.index - 1);
            MovePanelAddPokemon(-1920);
            MovePanelPokemon(-1920);
            MovePanelShowInfo(0);

        });
        TopY = (((Position * templateHeight) + (20 * (Position + 1))) * -1);
        item.Parent(ContentListPokemon);
        item.SetRawImage(PokemonTexture[Position]);
        item.Location(TopY);
        item.transform.localScale = new Vector3(1f, 1f, 1f);
        var Height = ((ContentListPokemon.childCount * templateHeight) + (20 * (Position + 1)));
        Debug.Log(Height);
        ContentListPokemon.sizeDelta = new Vector2(ContentListPokemon.sizeDelta.x, Height);
        Debug.Log(ContentListAddPokemon.sizeDelta);
    }

    public void UpdateListAddPokemon(Pokemon pokemon)
    {
        int index = adapterListPokemons.FindIndex(poke => poke.Name == pokemon.Name);
        Debug.Log(index);
        var ContentRectTransform = AdapterListAddPokemon.GetComponent<RectTransform>();
        float templateHeight = ContentRectTransform.rect.height;
        for (int i = index; i < adapterListPokemons.Count; i++)
        {
            if (i == index & ContentListAddPokemon.childCount > 0)
            {
                DestroyImmediate(adapterListPokemons[i].gameObject);
                adapterListPokemons.RemoveAt(i);
            }

            if (ContentListAddPokemon.childCount > 0 ) 
            {
                adapterListPokemons[i].Location((((i * templateHeight) + (20 * (i + 1))) * -1));
            } 
        }
        
    }

    public void MovePanelAddPokemon(float PosX)
    {
        PanelAddPokemon.localPosition = new Vector2(PosX, 0);
    }

    public void MovePanelPokemon(float PosX)
    {
        PanelPokemon.localPosition = new Vector2(PosX, 0);
    }

    public void MovePanelShowInfo(float PosX)
    {
        PanelShowInfo.localPosition = new Vector2(PosX, 0);
    }
}
