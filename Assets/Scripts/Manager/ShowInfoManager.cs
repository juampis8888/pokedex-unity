using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShowInfoManager : MonoBehaviour
{
    public List<PokemonInfo> pokemonInfos;

    public TextMeshProUGUI NameText;

    public RawImage RawImageSprite;

    public RawImage RawImageStats;

    public RectTransform ContentAbility;

    public RectTransform ContentEvolution;

    public RectTransform ContentTypePokemon;

    public RectTransform ContentDebilityPokemon;

    public AdapterAbility AdapterAbility;

    public AdapterEvolution AdapterEvolution;

    public AdapterTypePokemon AdapterTypePokemon;

    public 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowInfoPokemon(int index)
    {
        var ContentAbilityRectTransform = AdapterAbility.GetComponent<RectTransform>();
        float templateWidthAbility = ContentAbilityRectTransform.rect.width;
        var ContentEvolutionRectTransform = AdapterEvolution.GetComponent<RectTransform>();
        float templateWidthEvolution = ContentEvolutionRectTransform.rect.width;
        var ContentTypePokemonRectTransform = AdapterTypePokemon.GetComponent<RectTransform>();
        float templateWidthTypePokemon = ContentTypePokemonRectTransform.rect.width;

        float TopX = 0;
        int Position = 0;
        NameText.text = string.Format("{0}. {1}", index + 1,pokemonInfos[index].Name);
        RawImageSprite.texture = pokemonInfos[index].Sprite;
        RawImageStats.texture = pokemonInfos[index].Stats;
        DeleteChildsContent();

        pokemonInfos[index].Ability.ForEach(ability => 
        {
            var item = Instantiate(AdapterAbility);
            item.name = ability;
            item.Name = ability;
            item.NameText.text = ability;

            TopX = (((Position * templateWidthAbility) + (100 * (Position + 1))));
            item.Parent(ContentAbility);
            item.Location(TopX);
            Debug.Log(TopX + " " + Position + " " + templateWidthAbility);
            item.transform.localScale = new Vector3(1f, 1f, 1f);
            Position++;
        });
        var Width = ((Position * templateWidthAbility) + (100 * (Position + 1)));
        Debug.Log(Width);
        ContentAbility.sizeDelta = new Vector2(Width, ContentAbility.sizeDelta.y);
        Position = 0;
        Width = 0;

        pokemonInfos[index].Evolution.ForEach(evolution =>
        {
            var item = Instantiate(AdapterEvolution);
            item.name = evolution;
            item.Name = evolution;
            item.NameText.text = evolution;

            TopX = (((Position * templateWidthEvolution) + (100 * (Position + 1))));
            item.Parent(ContentEvolution);
            item.Location(TopX);
            item.transform.localScale = new Vector3(1f, 1f, 1f);
            Position++;
        });
        Width = ((Position * templateWidthEvolution) + (100 * (Position + 1)));
        Debug.Log(Width);
        ContentEvolution.sizeDelta = new Vector2(Width, ContentEvolution.sizeDelta.y);
        Position = 0;
        Width = 0;

        pokemonInfos[index].TypePokemon.ForEach(typePokemon =>
        {
            var item = Instantiate(AdapterTypePokemon);
            item.name = typePokemon.NameType;
            item.Name = typePokemon.NameType;
            item.NameText.text = typePokemon.NameType;
            item.SpriteType.texture = typePokemon.TextureType;
            TopX = (((Position * templateWidthTypePokemon) + (100 * (Position + 1))));
            item.Parent(ContentTypePokemon);
            item.Location(TopX);
            item.transform.localScale = new Vector3(1f, 1f, 1f);
            Position++;
        });
        Width = ((Position * templateWidthTypePokemon) + (100 * (Position + 1)));
        Debug.Log(Width);
        ContentTypePokemon.sizeDelta = new Vector2(Width, ContentTypePokemon.sizeDelta.y);
        Position = 0;
        Width = 0;

        pokemonInfos[index].Debility.ForEach(debilityPokemon =>
        {
            var item = Instantiate(AdapterTypePokemon);
            item.name = debilityPokemon.NameType;
            item.Name = debilityPokemon.NameType;
            item.NameText.text = debilityPokemon.NameType;
            item.SpriteType.texture = debilityPokemon.TextureType;
            TopX = (((Position * templateWidthTypePokemon) + (50 * (Position + 1))));
            item.Parent(ContentDebilityPokemon);
            item.Location(TopX);
            item.transform.localScale = new Vector3(1f, 1f, 1f);
            Position++;
        });
        Width = ((Position * templateWidthTypePokemon) + (50 * (Position + 1)));
        Debug.Log(Width);
        ContentDebilityPokemon.sizeDelta = new Vector2(Width, ContentDebilityPokemon.sizeDelta.y);
        Position = 0;
        Width = 0;
    }

    public void DeleteChildsContent()
    {
        int Count = ContentAbility.childCount;
        if (Count > 0)
        {
            for(int i = 0; i < Count; i++)
            {
                Destroy(ContentAbility.GetChild(i).gameObject);
            }
        }

        Count = ContentEvolution.childCount;
        if (Count > 0)
        {
            for (int i = 0; i < Count; i++)
            {
                Destroy(ContentEvolution.GetChild(i).gameObject);
            }
        }

        Count = ContentTypePokemon.childCount;
        if (Count > 0)
        {
            for (int i = 0; i < Count; i++)
            {
                Destroy(ContentTypePokemon.GetChild(i).gameObject);
            }
        }

        Count = ContentDebilityPokemon.childCount;
        if (Count > 0)
        {
            for (int i = 0; i < Count; i++)
            {
                Destroy(ContentDebilityPokemon.GetChild(i).gameObject);
            }
        }
    }
}
